var webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
var path = require('path');

var cleanCSSLoader = {
    loader: 'clean-css-loader',
    options: {
        compatibility: 'ie9',
        level: 2,
        inline: ['remote']
    }
}

var sassLoader = {
    loader: 'sass-loader',
    options: {
        sourceMap: true
    }
}

var imageLoader = {
    loader: 'file-loader',
    options: {
        name: '[name].[ext]',
        publicPath: '',
        outputPath: '../images/',
        useRelativePath: false,
    }
}

var fontLoader = {
    loader: 'file-loader',
    options: {
        name: '[name].[ext]',
        publicPath: '',
        outputPath: '../fonts/',
        useRelativePath: false,
    }
}

module.exports = function (env) {
    return {
        entry: {
            main: "./src/js/app.js",
            style: "./src/scss/style.scss"
        },
        output: {
            path: __dirname + '/dist/js',
            filename: "[name].js"
        },
        mode: 'development',
        module: {
            rules: [{
                    test: /\.js?$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    options: {
                        presets: [
                            ['@babel/env', {
                                targets: {
                                    browsers: ['last 2 versions']
                                }
                            }]
                        ]
                    }
                },
                {
                    test: /\.css$/,
                    use: [{
                        loader: MiniCssExtractPlugin.loader
                    }, 'css-loader'],
                    exclude: [/node_modules/],
                },
                {
                    test: /\.scss$/,
                    use: [{
                        loader: MiniCssExtractPlugin.loader
                    }, 'css-loader', cleanCSSLoader, 'resolve-url-loader', sassLoader],
                    exclude: [/node_modules/],
                },
                {
                    test: /\.png($|\?)|\.jpg($|\?)|\.jpeg($|\?)|\.gif($|\?)|\.svg($|\?)/,
                    use: [imageLoader],
                    exclude: [/fonts/]
                },
                {
                    test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                    use: [fontLoader],
                    exclude: [/images/]
                }
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                'window.jQuery': 'jquery',
            }),
            new MiniCssExtractPlugin({
                filename: '../styles/[name].css',
            }),
            // new BrowserSyncPlugin({
            //     host: 'localhost',
            //     port: 3000,
            //     // proxy: 'http://localhost:9000',
            //     server: { baseDir: ['dist'] },
            //     files: [{
            //         match: [
            //             '**/*.*'
            //         ],
            //         fn: function(event, file) {
            //             if (event === "change") {
            //                 const bs = require('browser-sync').get('bs-webpack-plugin');
            //                 bs.reload();
            //             }
            //         }
            //     }]
            // }, {
            //     // prevent BrowserSync from reloading the page 
            //     // and let Webpack Dev Server take care of this 
            //     reload: false
            // })
        ],
        // optimization: {
        //     splitChunks: {
        //         cacheGroups: {
        //             style: {
        //                 name: 'style',
        //                 test: /style\.scss$/,
        //                 chunks: 'all',
        //                 enforce: true
        //             }
        //         }
        //     }
        // },
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            port: 9000
        }
    }
}