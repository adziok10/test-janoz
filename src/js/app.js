import 'owl.carousel';
import 'owl.carousel2.thumbs';


$(document).ready(() => {
    $('.owl-carousel').owlCarousel({
        loop: true,
        items: 1,
        thumbs: true,
        thumbImage: true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item',
        nav: true,
        navText: ["<i class='icon-left-arrow'></i>","<i class='icon-right-arrow'></i>"]
    });
});